package com.ri.gateway.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtilsTest {

  /**
   * Check if the date that we return is in the correct format.
   */
  @Test
  public void getFormattedDateTest() {
    String timestamp = DateUtils.getFormattedDate(new Date());
    String expectedFormat = "yyyy-MM-dd HH:mm:ss";
    assertTrue(isValidFormat(expectedFormat, timestamp));
  }

  @Test
  public void getFormattedDateStringFormatTest() {
    String timestamp = DateUtils.getFormattedDate("20160519001300", "yyyyMMddHHmmss");
    String expectedFormat = "yyyy-MM-dd HH:mm:ss";
    assertTrue(isValidFormat(expectedFormat, timestamp));
  }
  
  @Test
  public void getFormattedDateStringTest() {
    String timestamp = DateUtils.getFormattedDate("20160519001300", "yyyyMMddHHmmss");
    assertEquals("2016-05-19 00:13:00", timestamp);
    
    timestamp = DateUtils.getFormattedDate("20160519121300", "yyyyMMddHHmmss");
    assertEquals("2016-05-19 12:13:00", timestamp);
  }
  
  private boolean isValidFormat(String format, String value) {
    Date date = null;
    try {
      SimpleDateFormat sdf = new SimpleDateFormat(format);
      date = sdf.parse(value);
      if (!value.equals(sdf.format(date))) {
        date = null;
      }
    } catch (ParseException ex) {
      ex.printStackTrace();
    }
    return date != null;
  }

  
}
