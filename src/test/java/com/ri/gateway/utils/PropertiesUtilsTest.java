package com.ri.gateway.utils;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

//Ensure that we can read from the environment.properties file
public class PropertiesUtilsTest {

  PropertiesUtils properties;

  @Before
  public void setUp() {
    this.properties = new PropertiesUtils();
  }

  @Test
  public void testGetProjectId() {
    assertNotNull(properties.getProjectId());
  }

  @Test
  public void testGetStagingLocation() {
    assertNotNull(properties.getStagingLocation());
  }

  @Test
  public void testGetTopic() {
    assertNotNull(properties.getTopic());
  }

  @Test
  public void testGetNumWorkers() {
    assertNotNull(properties.getNumWorkers());
  }
  
}