package com.ri.gateway.pipeline;

import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.io.PubsubIO;
import com.google.cloud.dataflow.sdk.options.DataflowPipelineOptions;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.values.PCollection;

import com.ri.gateway.pipeline.transform.DatastoreTransform;
import com.ri.gateway.utils.PropertiesUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Google Dataflow Pipeline for the Rentokil Initial Cloud Connect server.
 * 
 * <p>This Pipeline performs a streaming read from Pubsub and then runs a number of
 * parallel transforms to store data in both Google Datastore and Google
 * BigQuery.
 * 
 * <p>The transform and write to Google Datastore is processed in the following
 * fashion:
 * <li>ReadFromPubsub => TransformStringToEntity => WriteToDatastore
 *
 * <p>The transform and write to Google BigQuery is processed using the following
 * pipeline:
 * <li>ReadFromPubsub => WindowDataIntoMonths => SplitPcDataIntoTableRows =>
 * WritesToBigQuery
 *
 * @author LONAH28
 */
@SuppressWarnings("serial")
public class ConnectPipeline {

  private static final Logger LOGGER = LoggerFactory
      .getLogger(ConnectPipeline.class);

  private static final String BQ_DATE_FORMAT = "yyyy_MM";

  /**
   * Parses a string and returns the boolean value.
   * 
   * @param updateString
   *          from the command line.
   * @return true if the pipeline needs updating, false if it is to create a new
   *         pipeline.
   */
  public static boolean getUpdateFlag(String updateString) {
    if (updateString.equalsIgnoreCase("true")) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Google Dataflow pipeline for Cloud Connect.
   * 
   * <p>To run the application and launch the Google Dataflow Pipeline, run the
   * following command:
   * 
   * <p>mvn clean install compile -P ENV_NAME exec:java
   * -Dexec.mainClass=com.ri.gateway.pipeline.ConnectPipeline
   * -Dupdate=UPDATE_FLAG
   * 
   * <p>ENV_NAME refers to one of the target environments to build to:
   * <li>dev
   * <li>test
   * <li>stage
   * <li>production
   * 
   * <p>UPDATE_FLAG is either true or false, where true will update an existing
   * pipeline and false will create a new pipeline.
   */
  public static void main(String[] args) {
    //PropertiesUtils propertiesUtils = new PropertiesUtils();

    boolean updateFlag = false;

    // Create and set the PipelineOptions.
    DataflowPipelineOptions options = ConnectPipelineOptions
        .getOptions(updateFlag);

    // Create the pipeline
    Pipeline pipeline = Pipeline.create(options);

    LOGGER.info("Starting Dataflow Pipeline");
    PCollection<String> input = pipeline.apply(PubsubIO.Read.named(
        "ReadFromPubsub").topic("projects/dev-pnol-endpoint/topics/recieve"));

    input.apply(ParDo.named("WriteToDatastore").of(
        new DatastoreTransform()));


    

    pipeline.run();

  }
}
