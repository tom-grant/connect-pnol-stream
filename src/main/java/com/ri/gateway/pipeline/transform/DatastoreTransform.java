package com.ri.gateway.pipeline.transform;

import com.google.cloud.dataflow.sdk.transforms.DoFn;
import com.google.cloud.datastore.DatastoreException;
import com.google.cloud.datastore.Entity;

import com.ri.gateway.datastore.model.Account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Stores a PCollection Entity in the Google Cloud Datastore.
 */
public class DatastoreTransform extends DoFn<String, Entity> {

  /**
   * DatastoreTransform seriablizableUID.
   */
  private static final long serialVersionUID = 8359310500451140792L;

  private static final Logger LOGGER = LoggerFactory
      .getLogger(DatastoreTransform.class);

  /**
   * Processes each Entity in our PCollection Entity and stores them in the
   * Google Cloud Datastore.
   */
  @Override
  public void processElement(ProcessContext context) throws IOException,
      DatastoreException {

    Account account = new Account();
    Entity entity = account.makePcDataEntity(context.element());

    try {
      account.writeToDatastore(entity);
    } catch (DatastoreException datastoreException) {
      LOGGER.warn("Problem writing Entity to datastore");
    } catch (Exception e) {
      LOGGER.error("Unexpected error writing to datastore");
    }
    context.output(entity);
  }
}