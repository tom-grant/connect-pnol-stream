package com.ri.gateway.pipeline;

import com.google.cloud.dataflow.sdk.options.DataflowPipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.runners.DataflowPipelineRunner;

import com.ri.gateway.utils.PropertiesUtils;

/**
 * DataflowPipelineOptions class representing the execution conditions 
 * in which the Google Dataflow Pipeline will run.
 * 
 * @author LONAH28
 */
public class ConnectPipelineOptions {
  
  /**
   * Machine type to use in GCE.
   */
  private static final String MACHINE_TYPE = "n1-standard-2";

  /**
   * Name of the Dataflow job.
   */
  private static final String JOB_NAME = "pnol-endpoint-dataflow";

  /**
   * Zone to run the GCE instances in.
   */
  private static final String ZONE = "europe-west1-b";
  
  /**
   * Set pipeline to streaming mode.
   */
  //TODO: set to streaming after dev complete
  private static final boolean IS_STREAMING = true;

  /**
   * Sets up the DataflowPipelineOptions for Google Dataflow.
   * 
   * @return DataflowPipelineOptions containing project setup
   */
  public static DataflowPipelineOptions getOptions(boolean updateFlag) {
    PropertiesUtils propertiesUtils = new PropertiesUtils();

    DataflowPipelineOptions options = PipelineOptionsFactory
        .as(DataflowPipelineOptions.class);
    options.setProject(propertiesUtils.getProjectId());
    options.setStagingLocation(propertiesUtils.getStagingLocation());
    options.setStreaming(IS_STREAMING);
    options.setRunner(DataflowPipelineRunner.class);
    options.setNumWorkers(propertiesUtils.getNumWorkers());
    options.setWorkerMachineType(MACHINE_TYPE);
    options.setJobName(JOB_NAME);
    options.setZone(ZONE);
    options.setUpdate(updateFlag);

    return options;
  }
}
