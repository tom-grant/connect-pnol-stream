package com.ri.gateway.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

  private static final Logger LOGGER = LoggerFactory.getLogger(DateUtils.class);
  
  public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
  public static final String MONTH_YEAR_FORMAT = "MMyyyy";

  /**
   * Generates current date/time in the default date/time format yyyy-MM-dd
   * HH:mm:ss.
   * 
   * @return formatted string with date and time
   */
  public static String getFormattedDate(Date date) {
    return formatDate(date, DEFAULT_DATE_FORMAT);
  }

  /**
   * Generates the current date/time based on a user inputted dateFormat.
   * 
   * @param dateString
   *          string representation of a date
   * @param dateFormat
   *          desired output format
   * @return timestamp formatted based on the output format
   */
  public static String getFormattedDate(String dateString, String dateFormat) {
    SimpleDateFormat inputFormat = new SimpleDateFormat(dateFormat,
        Locale.ENGLISH);
    Date date;
    try {
      date = inputFormat.parse(dateString);
      return formatDate(date, DEFAULT_DATE_FORMAT);
    } catch (ParseException e) {
      LOGGER.warn("Unable to parse date");
      e.printStackTrace();
    }
    date = new Date();
    return formatDate(date, DEFAULT_DATE_FORMAT);
  }

  /**
   * Generates the current date/time based on a user inputted dateFormat.
   * 
   * @param format
   *          of the date/time to return.
   * @return date/time formatted based on user's input.
   */
  public static String getFormattedDate(Date date, String dateFormat) {
    return formatDate(date, dateFormat);
  }

  private static String formatDate(Date date, String dateFormat) {
    SimpleDateFormat ft = new SimpleDateFormat(dateFormat);
    String formattedDate = ft.format(date);
    return formattedDate;
  }

}
