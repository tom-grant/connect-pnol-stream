package com.ri.gateway.utils;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for reading from environment.properties.
 * 
 * <p>
 * This class contains environment specific information to deploy to different
 * Google Cloud Platform projects
 * </p>
 * 
 * @author LONAH28
 */
public class PropertiesUtils {

  private static final Logger LOG = LoggerFactory
      .getLogger(PropertiesUtils.class);

  private Configuration config;
  private static final String PROJECT_ID = "dataflow.options.projectid";
  private static final String STAGING_LOCATION = "dataflow.options.staginglocation";
  private static final String TOPIC = "dataflow.options.topic";
  private static final String NUM_WORKERS = "dataflow.options.numworkers";
  private static final String SERVICE_ACCOUNT = "dataflow.options.serviceAccount";
  private static final String SERVICE_KEY = "dataflow.options.serviceKey";
  private static final String BIG_QUERY_DATASET = "dataflow.options.bigqueryqualifier";

  /**
   * Constructor for the PropertiesUtils class which sets the input file.
   */
  public PropertiesUtils() {
    try {
      config = new PropertiesConfiguration("environment.properties");
    } catch (ConfigurationException exception) {
      LOG.warn("Unable to load environment.properties file");
      exception.printStackTrace();
    }
  }

  /**
   * Returns the ProjectID of the Google Environment.
   * 
   * @return projectId
   */
  public String getProjectId() {
    return config.getString(PROJECT_ID);
  }

  /**
   * Returns the GCS bucket for staging files.
   * 
   * @return GCS location
   */
  public String getStagingLocation() {
    return config.getString(STAGING_LOCATION);
  }

  /**
   * Returns the PubSub topic.
   * 
   * @return name of the PubSub topic
   */
  public String getTopic() {
    return config.getString(TOPIC);
  }

  /**
   * Returns the number of worker nodes.
   * 
   * @return number of worker nodes
   */
  public int getNumWorkers() {
    return config.getInt(NUM_WORKERS);
  }
  
  
  /**
   * Returns the service account to build project.
   * 
   */
  public String getServiceAccount() {
    return config.getString(SERVICE_ACCOUNT);
  }
  
  /**
   * Returns the service key to authenticate project.
   * 
   */
  public String getServiceKey() {
    return config.getString(SERVICE_KEY);
  }
  
  /**
   * Returns the big query table, dataset and project.
   */
  public String getBigQueryDataset() {
    return config.getString(BIG_QUERY_DATASET);
  }
}