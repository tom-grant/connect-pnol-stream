package com.ri.gateway.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ConversionUtilsTest {

  
  @Test
  public void converthhmmToMinutesTest() {
    
    int expectedMinutes = 1080;
    assertEquals(expectedMinutes, ConversionUtils.converthhmmToMinutes("1800"));
  
    // short string will return -1 as index error will be caught.
    assertEquals(-1 , ConversionUtils.converthhmmToMinutes("180"));
      
    // String with characters will return -1 as index error, as cannot
    // convert to integer.
    assertEquals(-1 , ConversionUtils.converthhmmToMinutes("AAAA"));
    
  }
  
  @Test
  public void convertHexToIntegerTest() {
    
    // valid hex conversion
    assertEquals(171, ConversionUtils.hexToIntegerConversion("AB"));
    
    // valid hex conversion with a minus symbol
    assertEquals(-171, ConversionUtils.hexToIntegerConversion("-AB"));
    
    // Dealing with unexpected characters will be caught by
    // number format exception and return -1
    assertEquals(-1, ConversionUtils.hexToIntegerConversion("@*&!AB"));
  }
  
}
