/**
 * Contains the classes necessary to transform the following
 * reports received from a control panel:
 * <li>&lt;CP&gt;: control panel report
 * <li>&lt;RR&gt;: repeater report
 * <li>&lt;SR&gt;: sensor report
 * <li>&lt;MR&gt;: modem report
 * <li>&lt;CER&gt;: control panel extended report.
 * 
 * <p>The transforms contained within this package take a 
 * PCollection of type String and outputs a PCollection
 * of TableRows for storage within BigQuery.
 * 
 * @author LONAH28
 */
package com.ri.gateway.pipeline.transform;