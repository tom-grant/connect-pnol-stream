package com.ri.gateway.pubsub;

import static org.junit.Assert.assertEquals;

import com.ri.gateway.pubsub.PubsubParser;

import org.junit.Test;

public class PubsubParserTest {

  // Message returned if no <login>...</login> tags are found
  private static final String NO_LOGIN = "NO LOGIN FOUND";

  @Test
  public void testGetLogin() {
    String input = "<login>91284894928484</login><data>testoijeoj</data>";
    assertEquals("Check that the method returns the login ID: ",
        "91284894928484", PubsubParser.getMessageLogin(input));

    input = "91284894928484</login><data>testoijeoj</data>";
    assertEquals("Check that the method reports that no login ID was "
        + "found when the tag is incomplete: ", NO_LOGIN,
        PubsubParser.getMessageLogin(input));

    input = "<data>testoijeoj</data>";
    assertEquals("Check that the method reports that no login ID was "
        + "found when the <login> tag does not exist: ", NO_LOGIN,
        PubsubParser.getMessageLogin(input));

    input = "<data>testoijeoj</data><login>91284894928484</login>";
    assertEquals("Check that the method returns the login ID when the "
        + "login tag is not at the beginning: ", "91284894928484",
        PubsubParser.getMessageLogin(input));
  }

  @Test
  public void testGetIncomingXmlData() {
    String input = "<login>91284894928484</login><data>testoijeoj</data>";
    assertEquals("Check that the method returns the XML data: ",
        "<data>testoijeoj</data>",
        PubsubParser.getMessageBody(input));

    input = "<data>testoijeoj</data>";
    assertEquals("Check that the method returns the XML data "
        + "when no login tag exists: ", "<data>testoijeoj</data>",
        PubsubParser.getMessageBody(input));

    input = "";
    assertEquals("Check that the method returns the XML data "
        + "when no login tag exists: ", "",
        PubsubParser.getMessageBody(input));

    input = "<data>testoijeoj</data><login>91284894928484</login>";
    assertEquals("Check that the method returns the XML data "
        + "when the login tag is at the end of the file: ",
        "<data>testoijeoj</data>",
        PubsubParser.getMessageBody(input));

  }

  @Test
  public void testGetReturnXmlData() {
    String input = "<xml><cc><t>2016022513565139</t></cc><di><b>90115</b><i>FF0113</i></di></xml>";
    assertEquals(
        "Return xml response when no other tags are found",
        "<xml><cc><t>2016022513565139</t></cc><di><b>90115</b><i>FF0113</i></di></xml>",
        PubsubParser.getMessageResponse(input));

    input = "<cr><p>903150900017320000107150554014B</p></cr><xml><cc><t></t></cc></xml>";
    assertEquals("Return xml response, omitting all other tags",
        "<xml><cc><t></t></cc></xml>", PubsubParser.getMessageResponse(input));

  }

  @Test
  public void testSeparateReturnAndResponseXmlData() {
    String incomingMessage = "<sr><p>9991500000443201602251211000647</p>"
        + "<s>9011500019355000A0103391392000002FF0C0A07</s><d>192331</d></sr><sr><p>99915"
        + "00000443201602251211000647</p><s>9011500019355000A01033B0001000000FF0C0A0E</s>"
        + "<d>192331</d></sr><sr><p>9991500000443201602251211000647</p><s>901150001935500"
        + "0A01033B03E9000001FF0C0AED</s><d>192331</d></sr>";

    String returnMessage = "<xml><cc><t>201602251211463f</t></cc><di><b>9011500019355</b><i>FF0113"
        + "</i></di><di><b>9011500019355</b><i>FF0113</i></di></xml>";

    assertEquals("Only the incoming message is returned",
        PubsubParser
            .getMessageBody(incomingMessage + returnMessage),
        incomingMessage);

    assertEquals("Only the outgoing message is returned",
        PubsubParser.getMessageResponse(incomingMessage + returnMessage),
        returnMessage);
  }

  @Test
  public void testNoTagFoundInResponseXmlData() {
    assertEquals("Return empty string when no return xml found",
        PubsubParser.getMessageResponse(""), "");
  }

  @Test
  public void hasFailedChecksumTest() {
    String message = "<cr><p>903150900017320000107150554014B</p></cr><xml><cc><t></t></cc></xml>";
    boolean hasFailsedChecksum = PubsubParser.hasFailedChecksum(message);

    assertEquals(
        "Check that the message above does not ouput a checksum failure message",
        false, hasFailsedChecksum);

    message = "<cr><p>903150900017320000107150554014B</p></cr><xml><cc><t></t><f></f></cc></xml>";
    hasFailsedChecksum = PubsubParser.hasFailedChecksum(message);
    assertEquals(
        "Check that the message above does ouput a checksum failure message",
        true, hasFailsedChecksum);
  }
}
