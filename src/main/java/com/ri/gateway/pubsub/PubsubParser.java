package com.ri.gateway.pubsub;

import com.ri.gateway.pipeline.ConnectPipeline;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PubsubParser {

  private static final Logger LOG = LoggerFactory
      .getLogger(ConnectPipeline.class);

  /**
   * Parses the Pubsub message for the Login tags and returns the barcode from
   * between the tags.
   * 
   * @param message
   *          from PubSub
   * @return Login barcode
   */
  public static String getMessageLogin(String message) {
    final Pattern pattern = Pattern.compile("<login>(.+?)</login>");
    Matcher matcher = pattern.matcher(message.toLowerCase());
    matcher.find();

    try {
      return matcher.group(1);
    } catch (Exception exception) {
      LOG.info("No <login> tags exist in the message");
      return "NO LOGIN FOUND";
    }
  }

  /**
   * Parses the Pubsub message for the Login tags and returns the barcode from
   * between the tags.
   * 
   * @param message
   *          from PubSub
   * @return boolean indicating whether the data failed its checksum
   */
  public static boolean hasFailedChecksum(String message) {

    String xmlResponse = getMessageResponse(message);

    final Pattern pattern = Pattern.compile("<f></f>");
    Matcher matcher = pattern.matcher(xmlResponse.toLowerCase());
    matcher.find();

    try {
      // The XML response message contains an <f> tag, so it has failed the
      // checksum
      return !matcher.group(0).isEmpty();
    } catch (Exception exception) {
      return false;
    }
  }

  /**
   * Removes the Login tags from the XML payload and produces a String
   * containing just the data. Remove control panel response message,
   * represented by xml tags as we dont want to store this in pc_data
   * 
   * @param message
   *          from PubSub
   * @return String containing the XML payload
   */
  public static String getMessageBody(String message) {
    // check if contains xml response to delete. Not applicable to Cage.
    if (message.contains("<xml>")) {
      message = message.substring(0, message.indexOf("<xml>"));
    }
    return message.replaceAll("(?s)<login>.*?</login>", "");
  }

  /**
   * return any xml response data found.
   * 
   * @param message
   *          read from from PubSub
   * @return response message to a control panel
   */
  public static String getMessageResponse(String message) {

    final Pattern pattern = Pattern.compile("<xml>(.+?)</xml>");
    Matcher matcher = pattern.matcher(message);
    matcher.find();

    try {
      String xmlReturn = matcher.group(1);
      return "<xml>" + xmlReturn + "</xml>";
    } catch (Exception exception) {
      return "";
    }
  }

  public static String xmlFieldParser(String message, String fieldName){

    final Pattern pattern = Pattern.compile("<field name='"+fieldName+"'>(.+?)</field>");
    Matcher matcher = pattern.matcher(message);
    matcher.find();

    try {
      if(matcher.group(1).contains("</field>")){
        return "";
      }
      else {
        return matcher.group(1);
      }
    } catch (Exception exception) {
      return "";
    }
  }

  public static String xmlGetCountryCode(String message){

    final Pattern pattern = Pattern.compile("<country>(.+?)</country>");
    Matcher matcher = pattern.matcher(message);
    matcher.find();


    try {
      return matcher.group(1);
    } catch (Exception exception) {
      return "";
    }
  }
}
