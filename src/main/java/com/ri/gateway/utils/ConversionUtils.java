package com.ri.gateway.utils;

/**
 * For converting HEX and Decimal values of XML to human-readable forms.
 * 
 * @author LONRP16
 *
 */
public class ConversionUtils {

  /**
   * Converts a hexadecimal string to an integer.
   * 
   * @param hexValue
   *          contains the hexadecimal string.
   * @return the conversion of hexValue to an integer.
   */
  public static int hexToIntegerConversion(String hexValue) {
    try {
      return Integer.parseInt(hexValue.trim(), 16);
    } catch (NumberFormatException formatException) {
      formatException.printStackTrace();
    }
    return -1;
  }
  
  /**
   * Convert hhmm timestamp to minutes.
   * @param hhmm
   *        timestamp from a device
   */
  public static int converthhmmToMinutes(String hhmm) {
    try {
      int hours = Integer.parseInt(hhmm.substring(0, 2));
      int mins = Integer.parseInt(hhmm.substring(2, 4));
    
      return (hours * 60 ) + mins;
    
    } catch (Exception e) {
      return -1;
    }
  }

  /**
   * Converts an integer to a string.
   * 
   * @param inputString
   *          contains the string to convert.
   * @return integer value of the inputString.
   */
  public static int integerToString(String inputString) {
    try {
      return Integer.parseInt(inputString);
    } catch (NumberFormatException formatException) {
      formatException.printStackTrace();
      return -1;
    }
  }

}
