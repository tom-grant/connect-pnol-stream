package com.ri.gateway.datastore.model;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.Key;
import com.google.cloud.datastore.KeyFactory;
import com.google.cloud.datastore.StringValue;

import com.ri.gateway.pubsub.PubsubParser;
import com.ri.gateway.utils.DateUtils;

import com.ri.gateway.utils.PropertiesUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class Account {

  private static final Logger LOGGER = LoggerFactory
      .getLogger(Account.class);

  /**
   * Datastore entity kind.
   */
  private static final String PC_DATA_KIND = "Account";

  /**
   * The name of the instance that the data was received from.
   */
  public static final String NAMESPACE = "pnol";

  /**
   * Entity properties.
   */
  public static final String COUNTRY_CODE = "country_code";
  public static final String BUSINESS_CODE = "business_code";
  public static final String ACCOUNT_NUMBER = "account_number";
  public static final String ACCOUNT_NAME = "account_name";
  public static final String ACCOUNT_ADDRESS_LINE_1 = "account_address_line_1";
  public static final String ACCOUNT_ADDRESS_LINE_2 = "account_address_line_2";
  public static final String ACCOUNT_ADDRESS_LINE_3 = "account_address_line_3";
  public static final String ACCOUNT_ADDRESS_LINE_4 = "account_address_line_4";
  public static final String ACCOUNT_ADDRESS_LINE_5 = "account_address_line_5";
  public static final String ACCOUNT_POSTCODE = "account_postcode";
  public static final String ACCOUNT_CONTACT_TELEPHONE = "account_contact_telephone";

  /**
   * XML fields
   */
  private static final String businessCode = "BusinessCode";
  private static final String accountNumber = "AccountNumber";
  private static final String accountName = "AccountName";
  private static final String accountAddressLine1 = "AccountAddressLine1";
  private static final String accountAddressLine2 = "AccountAddressLine2";
  private static final String accountAddressLine3 = "AccountAddressLine3";
  private static final String accountAddressLine4 = "AccountAddressLine4";
  private static final String accountAddressLine5 = "AccountAddressLine5";
  private static final String accountPostcode = "AccountPostcode";
  private static final String accountContactTelephone = "AccountContactTelephone";

  private static Datastore datastore;
  private PropertiesUtils propertiesUtils;

  /**
   * Create an authorized Datastore service using Application Default
   * Credentials.
   */
  public Account() {
    propertiesUtils = new PropertiesUtils();
    datastore = DatastoreOptions.builder().namespace(NAMESPACE)
        .projectId(propertiesUtils.getProjectId()).build().service();
  }

  /**
   * Creates an entity from a given message. The message is expected to contain
   * a Login tag and a Data tag which are then separated into their respective
   * properties in the Entity.
   *
   * @param message from PubSub
   * @return Google Cloud Datastore Entity
   */
  public Entity makePcDataEntity(String message) {

    KeyFactory keyFactory = datastore.newKeyFactory().kind(PC_DATA_KIND);
    Key key = datastore.allocateId(keyFactory.newKey());
    Entity entity = Entity
        .builder(key)
        .set(COUNTRY_CODE,
            StringValue.builder(PubsubParser.xmlGetCountryCode(message)).build())
        .set(
            BUSINESS_CODE,
            StringValue.builder(PubsubParser.xmlFieldParser(message, businessCode))
                .build())
        .set(
            ACCOUNT_NUMBER,
            StringValue.builder(PubsubParser.xmlFieldParser(message, accountNumber))
                .build())
        .set(
            ACCOUNT_NAME,
            StringValue.builder(PubsubParser.xmlFieldParser(message, accountName))
                .excludeFromIndexes(true).build())
        .set(ACCOUNT_ADDRESS_LINE_1, StringValue.builder(PubsubParser.xmlFieldParser(message, accountAddressLine1)).excludeFromIndexes(true).build())
        .set(ACCOUNT_ADDRESS_LINE_2, StringValue.builder(PubsubParser.xmlFieldParser(message, accountAddressLine2)).excludeFromIndexes(true).build())
        .set(ACCOUNT_ADDRESS_LINE_3, StringValue.builder(PubsubParser.xmlFieldParser(message, accountAddressLine3)).excludeFromIndexes(true).build())
        .set(ACCOUNT_ADDRESS_LINE_4, StringValue.builder(PubsubParser.xmlFieldParser(message, accountAddressLine4)).excludeFromIndexes(true).build())
        .set(ACCOUNT_ADDRESS_LINE_5, StringValue.builder(PubsubParser.xmlFieldParser(message, accountAddressLine5)).excludeFromIndexes(true).build())
        .set(ACCOUNT_POSTCODE, StringValue.builder(PubsubParser.xmlFieldParser(message, accountPostcode)).build())
        .set(ACCOUNT_CONTACT_TELEPHONE, StringValue.builder(PubsubParser.xmlFieldParser(message, accountContactTelephone)).build())
        .build();

    LOGGER.info("Created Datastore Entity");
    return entity;
  }

  /**
   * Writes an Entity to the Google Cloud Datastore.
   *
   * @param entity Datstore Entity to write
   * @return an Entity with the same properties and a key that is either newly
   * allocated or the same one if key is already complete.
   */
  public Entity writeToDatastore(Entity entity) {
    return datastore.put(entity);
  }
}
